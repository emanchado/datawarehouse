# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Download logs from Beaker."""
import time
import logging
import xmlrpc.client
from functools import lru_cache
import re
import os
from requests_futures.sessions import FuturesSession
from urllib3.exceptions import RequestError
from requests.exceptions import RequestException
from sentry_sdk import capture_exception

from django.conf import settings


class BeakerLogsDownloader:
    """Download beaker logs to an external server."""

    def __init__(self, rc_data, pipeline):
        """Initialize."""
        self.rc_data = rc_data
        self.pipeline = pipeline
        self.xmlrpcserver = xmlrpc.client.ServerProxy(settings.BEAKER_URL)

    def beaker_get_logs_list(self, recipeset_id, task_id=None):
        """
        Get a list of logs from beaker.

        If no task_id is given, recipeset-level logs are returned.
        """
        logs = self.beaker_query_logs(f"R:{recipeset_id}")

        if task_id:
            regex = f".*recipes/{recipeset_id}/tasks/{task_id}/(logs|results).*" # noqa
        else:
            regex = f".*recipes/{recipeset_id}/logs.*"

        pattern = re.compile(regex)
        return [log['url'] for log in logs if pattern.search(log['url'])]

    @staticmethod
    def merge_duplicated_logs(logs_list):
        """Merge duplicated logs in the upload queue."""
        parsed = {}

        for log in logs_list:
            try:
                parsed[log.filename] += log
            except KeyError:
                parsed[log.filename] = log

        return parsed.values()

    @lru_cache(maxsize=None)
    def beaker_query_logs(self, beaker_id):
        """
        Get list of logs from beaker_id.

        beaker_id can be receipe, task, etc.
        Queries are cached.
        """
        return self.xmlrpcserver.taskactions.files(beaker_id)

    def get_logs_list(self, recipe_id, test=None):
        """Get list of BeakerLogfile from logs list."""
        logs_list = []
        for log_url in self.beaker_get_logs_list(recipe_id, test.task_id if test else None):
            logs_list.append(
                BeakerLogfile(log_url, self.pipeline, test)
            )
        return logs_list

    def download(self, recipe_id, test=None):
        """Download logs from recipe_id."""
        logs_list = self.get_logs_list(recipe_id, test)
        logs_downloaded = self.process_logs_async(logs_list)
        logs_downloaded = self.merge_duplicated_logs(logs_downloaded)
        return logs_downloaded

    def process_logs_async(self, logs, retry_n=0):
        """Run all the requests asyncronusly."""
        if retry_n > settings.REQUESTS_MAX_RETRIES:
            logging.error("Max retries exceeded (%s)", settings.REQUESTS_MAX_RETRIES)
            return []

        threads = []
        logs_downloaded = []
        session = FuturesSession(max_workers=settings.REQUESTS_MAX_WORKERS)
        for log in logs:
            if log.should_be_published:
                threads.append(
                    (log, session.get(log.url, timeout=settings.REQUESTS_TIMEOUT_S))
                )

        failed = []
        for log, session in threads:
            try:
                response = session.result()
            except (RequestError, RequestException) as error:
                capture_exception(error)
                logging.info("Request failed for %s", log.url)
                failed.append(log)
            else:
                log.content = response.content
                logs_downloaded.append(log)

        if failed:
            logging.info(
                "Will retry %s requests in %s seconds.",
                len(failed), settings.REQUESTS_TIME_BETWEEN_RETRIES_S
            )
            time.sleep(settings.REQUESTS_TIME_BETWEEN_RETRIES_S)
            logs_downloaded.extend(self.process_logs_async(failed, retry_n + 1))

        return logs_downloaded


class BeakerLogfile:
    """BeakerLogfile class."""

    def __init__(self, url, pipeline, testrun=None):
        """Initialize."""
        self.url = url
        self.pipeline = pipeline
        self.testrun = testrun
        self.name = os.path.basename(self.url)
        self.content = ''

    @property
    def should_be_published(self):
        """Check if log is elegible for publishing."""
        logs_to_publish = [
            'console.log',
            'resultoutputfile.log',
            'taskout.log',
            'dmesg.log',
            '.run.log',
            '.fail.log',
        ]

        return any([self.name.endswith(name) for name in logs_to_publish])

    @property
    def sanitized_content(self):
        """Check if we need to strip some parts of the file and remove them."""
        if self.name == 'console.log':
            kernel_version = self.pipeline.kernel_version.strip('.src.rpm')
            kernel_banner = f'Linux version {kernel_version}'

            regex = f'{re.escape(kernel_banner)}.*'.encode()
            matches = re.compile(regex).split(self.content)

            if len(matches) > 1:
                sanitized = kernel_banner.encode() + b''.join(matches[1:])
            else:
                sanitized = b'Empty log'
        else:
            sanitized = self.content

        return sanitized

    @property
    def filename(self):
        """Return a suitable file name for the log."""
        return self.name

    @staticmethod
    def sanitize_string(string):
        """Turn a string into a path-compatible name."""
        return "".join(
            char if char.isalnum() else '_' for char in string
            )

    def __add__(self, other):
        """Redefine add method."""
        self.content += b'\n' + other.content
        return self
