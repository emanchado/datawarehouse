# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.
"""Report models file."""

from django.db import models


class Recipient(models.Model):
    """Model for Recipient."""

    email = models.EmailField()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.email}'


class Report(models.Model):
    """Model for Report."""

    pipeline = models.ForeignKey('Pipeline',
                                 related_name='reports',
                                 on_delete=models.CASCADE)

    subject = models.CharField(max_length=200)
    msgid = models.CharField(max_length=200, unique=True)
    body = models.TextField()
    addr_to = models.ManyToManyField(Recipient, related_name='addr_to')
    addr_cc = models.ManyToManyField(Recipient, related_name='addr_cc')
    sent_at = models.DateTimeField()
    raw = models.TextField()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.pipeline.pipeline_id} - {self.msgid}'
