"""Lookup helpers."""
from django.db.models import Lookup
from django.db.models.fields import Field


@Field.register_lookup
class NotEqualLookup(Lookup):
    """Implement the SQL <> operator."""

    lookup_name = 'ne'

    def as_sql(self, compiler, connection):
        """Generate the sql query."""
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return '%s <> %s' % (lhs, rhs), params


@Field.register_lookup
class NotInLookup(Lookup):
    """Implement the SQL NOT IN operator.

    Adapted from django.db.models.lookups. Most probably only works for simple
    lists.
    """

    lookup_name = 'not_in'

    def process_rhs(self, compiler, connection):
        """Process in a loop."""
        sqls, sqls_params = self.batch_process_rhs(compiler, connection, self.rhs)
        placeholder = '(' + ', '.join(sqls) + ')'
        return (placeholder, sqls_params)

    def get_db_prep_lookup(self, value, connection):
        """Prepare in a loop."""
        return ('%s', [self.lhs.output_field.get_db_prep_value(v, connection, prepared=True) for v in value])

    def get_prep_lookup(self):
        """Prepare in a loop."""
        return [self.lhs.output_field.get_prep_value(v) for v in self.rhs]

    def as_sql(self, compiler, connection):
        """Generate the sql query."""
        self.rhs = set(self.rhs)
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return '%s NOT IN %s' % (lhs, rhs), params
