"""API Views."""
import mailbox
import base64
import datetime

import email.utils
from django.db.models import Q
from django.utils import timezone
from rest_framework import status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from datawarehouse import helpers, pagination, serializers, models, utils


class PipelineIssueRecord(APIView):
    """PipelineIssueRecord class."""

    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def post(self, request, pipeline_id):
        """Mark a pipeline's issue."""
        issue_id = request.data.get('issue_id')
        testrun_ids = request.data.get('testrun_ids', [])
        bot_generated = request.data.get('bot_generated', False)

        helpers.create_issue_record(issue_id, pipeline_id, testrun_ids, bot_generated)

        return Response(status=status.HTTP_201_CREATED)

    def delete(self, request, pipeline_id, record_id):
        """Delete IssueRecord from Pipeline."""
        try:
            issue_record = models.IssueRecord.objects.get(
                pipeline__pipeline_id=pipeline_id,
                id=record_id
            )
        except models.IssueRecord.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        issue_record.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, pipeline_id, record_id):
        """Edit testruns from IssueRecord."""
        issue_id = request.data.get('issue_id')
        testrun_ids = request.data.get('testrun_ids')
        bot_generated = request.data.get('bot_generated')

        try:
            issue_record = models.IssueRecord.objects.get(
                pipeline__pipeline_id=pipeline_id,
                id=record_id
            )
        except models.IssueRecord.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if issue_id is not None:
            issue_record.issue = models.Issue.objects.get(id=issue_id)

        if bot_generated is not None:
            issue_record.bot_generated = bot_generated

        issue_record.save()

        if testrun_ids is not None:
            testruns = models.TestRun.objects.filter(
                pipeline__pipeline_id=pipeline_id,
                id__in=testrun_ids
            )
            issue_record.testruns.set(testruns)

        return Response(status=status.HTTP_204_NO_CONTENT)


class PipelineByFailures(APIView):
    """Endpoint for pipeline failure handling."""

    def get(self, request, group):
        """Get a list of failed pipelines."""
        issues = helpers.get_failed_pipelines(group).filter(issue_records=None).exclude(duration=None)

        paginator = pagination.DatawarehousePagination()
        paginated_pipelines = paginator.paginate_queryset(issues, request)

        serialized_pipelines = serializers.TriagePipelineSerializer(paginated_pipelines, many=True).data

        context = {
            'pipelines': serialized_pipelines,
            'group': group
        }

        return paginator.get_paginated_response(context)


class IssueRecords(APIView):
    """Endpoint for Issue Records handling."""

    def get(self, request, issue_id):
        """Get a list of Issue Records."""
        try:
            issue = models.Issue.objects.get(id=issue_id)
        except models.Issue.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        records = models.IssueRecord.objects.filter(issue=issue)

        paginator = pagination.DatawarehousePagination()
        paginated_records = paginator.paginate_queryset(records, request)

        serialized_records = serializers.IssueRecordSerializer(paginated_records, many=True).data

        context = {
            'records': serialized_records,
        }

        return paginator.get_paginated_response(context)


class Pipeline(APIView):
    """Get pipeline data."""

    def get(self, request, pipeline_id):
        """Get information of a pipeline."""
        style = request.GET.get('style')

        try:
            pipe = models.Pipeline.objects.get(pipeline_id=pipeline_id)
        except models.Pipeline.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if style == 'reporter':
            serializer = serializers.PipelineOrderedSerializer
        else:
            serializer = serializers.PipelineSerializer

        return Response(serializer(pipe, context={'request': request}).data)


class PipelineReport(APIView):
    """Reports and pipelines."""

    def get(self, request, pipeline_id):
        """Get the reports of a pipeline."""
        try:
            pipeline = models.Pipeline.objects.get(pipeline_id=pipeline_id)
        except models.Pipeline.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        paginator = pagination.DatawarehousePagination()
        paginated_reports = paginator.paginate_queryset(
            pipeline.reports.all(),
            request)

        context = {
            'reports': serializers.ReportSerializer(paginated_reports, many=True).data,
        }

        return paginator.get_paginated_response(context)

    def post(self, request, pipeline_id):
        """Submit a new report for the pipeline."""
        content = request.data.get('content')
        mbox = mailbox.mboxMessage(content)

        addr_to = mbox.get_all('to', [])
        addr_cc = mbox.get_all('cc', [])
        subject = mbox.get('Subject')
        date = mbox.get('Date')
        msgid = mbox.get('Message-ID')
        body = mbox.get_payload()

        if models.Report.objects.filter(msgid=msgid).exists():
            return Response("MsgID already exists.", status=status.HTTP_400_BAD_REQUEST)

        if mbox.get('Content-Transfer-Encoding') == 'base64':
            body = base64.b64decode(body).decode()

        pipeline = models.Pipeline.objects.get(pipeline_id=pipeline_id)

        sent_at = email.utils.parsedate_to_datetime(date)
        try:
            sent_at = timezone.make_aware(sent_at)
        except ValueError:
            # tzinfo is already set
            pass

        report = models.Report.objects.create(
            pipeline=pipeline,
            subject=subject,
            body=body,
            msgid=msgid,
            sent_at=sent_at,
            raw=content,
        )

        for _, addr in email.utils.getaddresses(addr_to):
            recipient, _ = models.Recipient.objects.get_or_create(email=addr)
            report.addr_to.add(recipient)

        for _, addr in email.utils.getaddresses(addr_cc):
            recipient, _ = models.Recipient.objects.get_or_create(email=addr)
            report.addr_cc.add(recipient)

        return Response(status=status.HTTP_201_CREATED)


class ReportMissing(APIView):
    """Get pipeline without report sent."""

    def get(self, request):
        """Get list of pipelines without reports."""
        since = request.GET.get('since')

        # Exclude newer than 3 hours as grace time.
        # gitlab.cee.redhat.com/cki-project/deployment/datawarehouse-report-crawler
        last_3_hours = timezone.now() - datetime.timedelta(hours=3)

        pipelines = (models.Pipeline.objects.add_stats()
                     .filter(stats_report_exists=False)
                     .exclude(Q(duration=None) | Q(finished_at__gte=last_3_hours))
                     )

        if since:
            since = utils.timestamp_to_datetime(since)
            pipelines = pipelines.filter(created_at__gte=since)

        paginator = pagination.DatawarehousePagination()
        paginated_pipelines = paginator.paginate_queryset(pipelines, request)
        serialized_pipelines = serializers.PipelineSimpleSerializer(paginated_pipelines, many=True).data

        context = {
            'pipelines': serialized_pipelines,
        }

        return paginator.get_paginated_response(context)


class TestSigle(APIView):
    """Endpoint for handling single test."""

    def get(self, request, test_id):
        """Return a single test."""
        try:
            test = models.Test.objects.get(id=test_id)
        except models.Test.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serialized_test = serializers.TestSerializer(test).data

        context = {
            'test': serialized_test,
        }

        return Response(context)


class TestList(APIView):
    """Endpoint for handling many tests."""

    def get(self, request):
        """Return a list of all the tests."""
        tests = models.Test.objects.all()

        paginator = pagination.DatawarehousePagination()
        paginated_tests = paginator.paginate_queryset(tests, request)

        serialized_tests = serializers.TestSerializer(paginated_tests, many=True).data

        context = {
            'tests': serialized_tests,
        }

        return paginator.get_paginated_response(context)


class TestIssueRecord(APIView):
    """Issue records and tests."""

    def get(self, request, test_id):
        """Get list of issue records for a given test."""
        since = request.GET.get('since')

        try:
            test = models.Test.objects.get(id=test_id)
        except models.Test.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        issue_records = models.IssueRecord.objects.filter(testruns__test=test)

        if since:
            since = utils.timestamp_to_datetime(since)
            issue_records = issue_records.filter(pipeline__created_at__gte=since)

        paginator = pagination.DatawarehousePagination()
        paginated_issue_records = paginator.paginate_queryset(issue_records, request)
        serialized_issue_records = serializers.IssueRecordSerializer(paginated_issue_records, many=True).data

        context = {
            'issues': serialized_issue_records,
        }

        return paginator.get_paginated_response(context)


class TestStats(APIView):
    """Endpoint for handling test stats."""

    def get(self, request, test_id):
        """Return a test's stats."""
        since = request.GET.get('since')

        try:
            test = models.Test.objects.get(id=test_id)
        except models.Test.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        test_runs = test.testrun_set.all()
        if since:
            test_runs = test_runs.filter(pipeline__created_at__gte=since)

        issues = {}
        for issue_kind in models.IssueKind.objects.all():
            issue_records = models.IssueRecord.objects.filter(issue__kind=issue_kind, testruns__test=test)
            if since:
                issue_records = issue_records.filter(pipeline__created_at__gte=since)

            try:
                rate = issue_records.count() / test_runs.count()
            except ZeroDivisionError:
                rate = 0

            issues[issue_kind.tag] = {
                'reports': issue_records.count(),
                'rate': rate,
            }

        test_runs_failed = test_runs.filter(passed=False)

        context = {
            'test': serializers.TestSerializer(test).data,
            'total_runs': test_runs.count(),
            'total_failures': test_runs_failed.count(),
            'issues': issues,
        }
        return Response(context)
