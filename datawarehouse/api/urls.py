"""Urls file."""
from django.urls import path, include
from . import views

urlpatterns = [
    path('1/pipeline/<int:pipeline_id>/issue/record', views.PipelineIssueRecord.as_view()),
    path('1/pipeline/<int:pipeline_id>/issue/record/<int:record_id>', views.PipelineIssueRecord.as_view()),
    path('1/pipeline/<int:pipeline_id>/report', views.PipelineReport.as_view()),
    path('1/pipeline/<int:pipeline_id>', views.Pipeline.as_view()),
    path('1/pipeline/failures/<str:group>', views.PipelineByFailures.as_view()),
    path('1/issue/<int:issue_id>/record', views.IssueRecords.as_view()),
    path('1/report/missing', views.ReportMissing.as_view()),
    path('1/test/<int:test_id>/issue/record', views.TestIssueRecord.as_view()),
    path('1/test/<int:test_id>/stats', views.TestStats.as_view()),
    path('1/test/<int:test_id>', views.TestSigle.as_view()),
    path('1/test', views.TestList.as_view()),
    path('1/kcidb/', include('datawarehouse.api.kcidb.urls')),
]
