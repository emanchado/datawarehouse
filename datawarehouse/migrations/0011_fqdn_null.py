# Generated by Django 2.2.4 on 2019-09-03 17:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0010_add_ordering'),
    ]

    operations = [
        migrations.AlterField(
            model_name='beakerresource',
            name='fqdn',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
