from django.db import migrations, models
import django.db.models.deletion

# List of names extracted from
# https://docs.google.com/spreadsheets/d/1-uZjL22UCxfbPrRizLei0oEfOkF3SvP0TlOzh6-TGh0/edit#gid=0
names_change_list = [
    # Old, Intermediate, New
    ("ACPI table test", "/acpi/acpitable test - ACPI table test", "ACPI table test"),
    ("ACPI enabled test", "ACPI enabled test - ACPI enabled test", "ACPI enabled test"),
    ("Podman system integration test (as root)", "Podman system integration test - Podman system integration test (as root)", "Podman system integration test - as root"),
    ("Podman system integration test (as user)", "Podman system integration test - Podman system integration test (as user)", "Podman system integration test - as user"),
    ("CPU: Die Test", "CPU die testing - CPU: Die Test", "CPU: Die Test"),
    ("CPU: Frequency Driver Test", "CPU frequency driver testing - CPU: Frequency Driver Test", "CPU: Frequency Driver Test"),
    ("CPU: Idle Test", "CPU idle testing - CPU: Idle Test", "CPU: Idle Test"),
    ("debuginfo: vmlinux .debug sections test", "debuginfo: vmlinux .debug sections test - debuginfo: vmlinux .debug sections test", "debuginfo: vmlinux .debug sections test"),
    ("Basic example test", "Example test suite - Basic example test", "Example test suite - Basic example test"),
    ("Patch-targeted example test", "Example test suite - Patch-targeted example test", "Example test suite - Patch-targeted example test"),
    ("Baseline-only example test", "Example test suite - Baseline-only example test", "Example test suite - Baseline-only example test"),
    ("Non-baseline example test", "Example test suite - Non-baseline example test", "Example test suite - Non-baseline example test"),
    ("RHEL7-only example test", "Example test suite - RHEL7-only example test", "Example test suite - RHEL7-only example test"),
    ("AutoFS Connecathon: Server", "AutoFS Connectathon test - AutoFS Connecathon: Server", "AutoFS Connectathon - Server 1"),
    ("AutoFS Connecathon: Server", "AutoFS Connectathon test - AutoFS Connecathon: Server", "AutoFS Connectathon - Server 2"),
    ("AutoFS Connecathon: Client", "AutoFS Connectathon test - AutoFS Connecathon: Client", "AutoFS Connectathon - Client"),
    ("CIFS Connectathon", "CIFS connectathon - CIFS Connectathon", "CIFS Connectathon"),
    ("POSIX pjd-fstest suites", "pjd-fstest filesystem test - POSIX pjd-fstest suites", "POSIX pjd-fstest suites"),
    ("Loopdev Sanity", "sanity test for loop device - Loopdev Sanity", "Loopdev Sanity"),
    ("NFS Connectathon", "nfs connectathon - NFS Connectathon", "NFS Connectathon"),
    ("xfstests: ext4", "xfs tests - xfstests: ext4", "xfstests - ext4 - rhel7"),
    ("xfstests: ext4", "xfs tests - xfstests: ext4", "xfstests - ext4"),
    ("xfstests: xfs", "xfs tests - xfstests: xfs", "xfstests - xfs"),
    ("xfstests: xfs", "xfs tests - xfstests: xfs", "xfstests - xfs - rhel7"),
    ("fwts firmware test suite", "Firmware test suite - fwts firmware test suite", "Firmware test suite"),
    ("IOMMU boot test", "IOMMU boot test - IOMMU boot test", "IOMMU boot test"),
    ("IPMI driver test", "IPMI driver sanity test - IPMI driver test", "IPMI driver test"),
    ("IPMItool loop stress test", "IPMItool loop stress test - IPMItool loop stress test", "IPMItool loop stress test"),
    ("jvm test suite", "jvm test suite - jvm test suite", "jvm test suite"),
    ("kdump: sysrq-c", "kdump test suite - kdump: sysrq-c", "kdump - sysrq-c"),
    ("kdump: sysrq-c - megaraid_sas", "kdump test suite - kdump: sysrq-c - megaraid_sas", "kdump - sysrq-c - megaraid_sas"),
    ("kdump: sysrq-c - mpt3sas driver", "kdump test suite - kdump: sysrq-c - mpt3sas driver", "kdump - sysrq-c - mpt3sas driver"),
    ("PCI ID Removal Check", "PCI ID Removal Check - PCI ID Removal Check", "PCI ID Removal Check"),
    ("kABI Whitelist Symbol Removal Check", "kABI Whitelist Symbol Removal Check - kABI Whitelist Symbol Removal Check", "kABI Whitelist Symbol Removal Check"),
    ("LTP lite", "LTP lite test for RHEL kernels - LTP lite", "LTP lite"),
    ("LTP", "LTP test for upstream kernels - LTP", "LTP"),
    ("Memory function: kaslr", "memory/function/kaslr test - Memory function: kaslr", "Memory function: kaslr"),
    ("Memory function: memfd_create", "memory/function/memfd_create test - Memory function: memfd_create", "Memory function: memfd_create"),
    ("AMTU (Abstract Machine Test Utility)", "AMTU (Abstract Machine Test Utility) - AMTU (Abstract Machine Test Utility)", "AMTU (Abstract Machine Test Utility)"),
    ("Kernel Header Sanity Test", "kernel header sanity test - Kernel Header Sanity Test", "Kernel Header Sanity Test"),
    ("kernel-fips-mode test", "Test kernel FIPS 140 mode - kernel-fips-mode test", "kernel-fips-mode test"),
    ("LTP: openposix test suite", "LTP openposix testsuite - LTP: openposix test suite", "LTP: openposix test suite"),
    ("load/unload kernel module test", "load/unload kernel module test - load/unload kernel module test", "load/unload kernel module test"),
    ("Networking bridge: sanity", "Networking bridge sanity testing - Networking bridge: sanity", "Networking bridge: sanity"),
    ("Ethernet drivers sanity", "/networking/ethernet/drivers/sanity test - Ethernet drivers sanity", "Ethernet drivers sanity"),
    ("Networking firewall: basic netfilter test", "Networking firewall: basic netfilter test - Networking firewall: basic netfilter test", "Networking firewall: basic netfilter test"),
    ("Networking: igmp conformance test", "networking/igmp/conformance test - Networking: igmp conformance test", "Networking: igmp conformance test"),
    ("Networking ipsec: basic netns transport", "Networking ipsec: basic netns - Networking ipsec: basic netns transport", "Networking ipsec: basic netns - transport"),
    ("Networking ipsec: basic netns tunnel", "Networking ipsec: basic netns - Networking ipsec: basic netns tunnel", "Networking ipsec: basic netns - tunnel"),
    ("Networking: ipv6/Fujitsu-socketapi-test", "Networking: ipv6/Fujitsu-socketapi-test - Networking: ipv6/Fujitsu-socketapi-test", "Networking: ipv6/Fujitsu-socketapi-test"),
    ("kernel networking selftests: net", "kernel networking selftests - kernel networking selftests: net", "kernel networking selftests - net"),
    ("kernel networking selftests: bpf", "kernel networking selftests - kernel networking selftests: bpf", "kernel networking selftests - bpf"),
    ("kernel networking selftests: forwarding", "kernel networking selftests - kernel networking selftests: forwarding", "kernel networking selftests - forwarding"),
    ("kernel networking selftests: tc", "kernel networking selftests - kernel networking selftests: tc", "kernel networking selftests - tc"),
    ("Networking MACsec: sanity", "Networking MACsec sanity testing - Networking MACsec: sanity", "Networking MACsec: sanity"),
    ("Networking route: pmtu", "/networking/route/pmtu test - Networking route: pmtu", "Networking route: pmtu"),
    ("Networking route_func: local", "Networking route_func tests - Networking route_func: local", "Networking route_func - local"),
    ("Networking route_func: forward", "Networking route_func tests - Networking route_func: forward", "Networking route_func - forward"),
    ("Networking sctp-auth: sockopts test", "networking/sctp/auth/sockopts test - Networking sctp-auth: sockopts test", "Networking sctp-auth: sockopts test"),
    ("Networking socket: fuzz", "Networking socket: fuzz - Networking socket: fuzz", "Networking socket: fuzz"),
    ("Networking TCP: keepalive test", "networking/tcp/tcp_keepalive test - Networking TCP: keepalive test", "Networking TCP: keepalive test"),
    ("Networking tunnel: geneve basic test", "networking tunnel geneve basic test - Networking tunnel: geneve basic test", "Networking tunnel: geneve basic test"),
    ("Networking tunnel: gre basic", "networking tunnel gre basic - Networking tunnel: gre basic", "Networking tunnel: gre basic"),
    ("L2TP basic test", "L2TP basic test - L2TP basic test", "L2TP basic test"),
    ("Networking tunnel: vxlan basic", "Networking tunnel: vxlan basic test - Networking tunnel: vxlan basic", "Networking tunnel: vxlan basic"),
    ("Networking UDP: socket", "Networking UDP socket testing - Networking UDP: socket", "Networking UDP: socket"),
    ("Networking vnic: ipvlan/basic", "Networking vnic ipvlan basic testing - Networking vnic: ipvlan/basic", "Networking vnic: ipvlan/basic"),
    ("audit: audit testsuite test", "audit testsuite test - audit: audit testsuite test", "audit: audit testsuite test"),
    ("httpd: mod_ssl smoke sanity", "mod_ssl smoke sanity - httpd: mod_ssl smoke sanity", "httpd: mod_ssl smoke sanity"),
    ("i2c: i2cdetect sanity", "i2cdetect-smoke test - i2c: i2cdetect sanity", "i2c: i2cdetect sanity"),
    ("iotop: sanity", "iotop: sanity - iotop: sanity", "iotop: sanity"),
    ("net-snmp: tcp-transport test", "net-snmp: tcp-transport test - net-snmp: tcp-transport test", "net-snmp: tcp-transport test"),
    ("perf: internal-testsuite", "perf internal-testsuite - perf: internal-testsuite", "perf: internal-testsuite"),
    ("perf: perftool-testsuite", "perf perftool-testsuite - perf: perftool-testsuite", "perf: perftool-testsuite"),
    ("redhat-rpm-config: detect-kabi-provides sanity", "detect-kabi-provides - redhat-rpm-config: detect-kabi-provides sanity", "redhat-rpm-config: detect-kabi-provides sanity"),
    ("redhat-rpm-config: kabi-whitelist-not-found sanity", "kabi-whitelist-not-found - redhat-rpm-config: kabi-whitelist-not-found sanity", "redhat-rpm-config: kabi-whitelist-not-found sanity"),
    ("redhat-rpm-config: kernel-module-symbol-requires", "kernel-module-symbol-requires - redhat-rpm-config: kernel-module-symbol-requires", "redhat-rpm-config: kernel-module-symbol-requires"),
    ("selinux-policy: serge-testsuite", "selinux-policy serge-testsuite - selinux-policy: serge-testsuite", "selinux-policy: serge-testsuite"),
    ("tuned: tune-processes-through-perf", "tuned: tune-processes-through-perf - tuned: tune-processes-through-perf", "tuned: tune-processes-through-perf"),
    ("pciutils: sanity smoke test", "pciutils Sanity Smoke test - pciutils: sanity smoke test", "pciutils: sanity smoke test"),
    ("pciutils: update pci ids test", "pciutils update pci ids - pciutils: update pci ids test", "pciutils: update pci ids test"),
    ("power-management: cpufreq/cpufreq_governor test", "cpufreq/cpufreq_governor test - power-management: cpufreq/cpufreq_governor test", "power-management: cpufreq/cpufreq_governor test"),
    ("power-management: cpufreq/sys_cpufreq test", "cpufreq/sys_cpufreq test - power-management: cpufreq/sys_cpufreq test", "power-management: cpufreq/sys_cpufreq test"),
    ("power-management: cpupower/sanity test", "cpupower sanity test - power-management: cpupower/sanity test", "power-management: cpupower/sanity test"),
    ("power-management: rapl/powercap test", "Simple test of powercap - power-management: rapl/powercap test", "power-management: rapl/powercap test"),
    ("Power-Management: Suspend and Resume Test", "Suspend and Resume Test - Power-Management: Suspend and Resume Test", "Power-Management: Suspend and Resume Test"),
    ("RDMA Sanity Test IB - Server", "RDMA Sanity Test - IB - RDMA Sanity Test IB - Server", "RDMA Sanity Test - IB - Server"),
    ("RDMA Sanity Test IB - Client", "RDMA Sanity Test - IB - RDMA Sanity Test IB - Client", "RDMA Sanity Test - IB - Client"),
    ("RDMA Sanity Test iWARP - Server", "RDMA Sanity Test - iWARP - RDMA Sanity Test iWARP - Server", "RDMA Sanity Test - iWARP - Server"),
    ("RDMA Sanity Test iWARP - Client", "RDMA Sanity Test - iWARP - RDMA Sanity Test iWARP - Client", "RDMA Sanity Test - iWARP - Client"),
    ("RDMA Sanity Test OPA - Server", "RDMA Sanity Test - OPA - RDMA Sanity Test OPA - Server", "RDMA Sanity Test - OPA - Server"),
    ("RDMA Sanity Test OPA - Client", "RDMA Sanity Test - OPA - RDMA Sanity Test OPA - Client", "RDMA Sanity Test - OPA - Client"),
    ("RDMA Sanity Test RoCE - Server", "RDMA Sanity Test - RoCE - RDMA Sanity Test RoCE - Server", "RDMA Sanity Test - RoCE - Server"),
    ("RDMA Sanity Test RoCE - Client", "RDMA Sanity Test - RoCE - RDMA Sanity Test RoCE - Client", "RDMA Sanity Test - RoCE - Client"),
    ("kernel-rt: rteval", "kernel-rt: rteval - kernel-rt: rteval", "kernel-rt: rteval"),
    ("kernel-rt: rt_migrate_test", "kernel-rt: rt_migrate_test - kernel-rt: rt_migrate_test", "kernel-rt: rt_migrate_test"),
    ("kernel-rt: sched_deadline", "kernel-rt: sched_deadline - kernel-rt: sched_deadline", "kernel-rt: sched_deadline"),
    ("kernel-rt: smidetect", "kernel-rt: smidetect - kernel-rt: smidetect", "kernel-rt: smidetect"),
    ("Livepatch kselftests", "Livepatch kselftests - Livepatch kselftests", "Livepatch kselftests"),
    ("PowerPC self tests", "PowerPC self tests - PowerPC self tests", "PowerPC self tests - power9_baremetal"),
    ("PowerPC self tests", "PowerPC self tests - PowerPC self tests", "PowerPC self tests - power9_powervm"),
    ("PowerPC self tests", "PowerPC self tests - PowerPC self tests", "PowerPC self tests - power8_kvm"),
    ("PowerPC self tests", "PowerPC self tests - PowerPC self tests", "PowerPC self tests - power8_powervm"),
    ("PowerPC self tests", "PowerPC self tests - PowerPC self tests", "PowerPC self tests - power8_baremetal"),
    ("ALSA PCM loopback test", "ALSA PCM loopback test - ALSA PCM loopback test", "ALSA PCM loopback test"),
    ("ALSA Control (mixer) Userspace Element test", "ALSA Control (mixer) Userspace Element test - ALSA Control (mixer) Userspace Element test", "ALSA Control (mixer) Userspace Element test"),
    ("Usex - version 1.9-29", "Usex - version 1.9-29 - Usex - version 1.9-29", "Usex - version 1.9-29"),
    ("Storage blktests", "storage block tests - Storage blktests", "Storage blktests - rhel7"),
    ("Storage blktests", "storage block tests - Storage blktests", "Storage blktests"),
    ("storage: dm/common", "Storage tests device-mapper with parameters - storage: dm/common", "storage: dm/common"),
    ("Storage SAN device stress - megaraid_sas", "Storage SAN device stress tests - Storage SAN device stress - megaraid_sas", "Storage SAN device stress - megaraid_sas"),
    ("Storage SAN device stress - mpt3sas driver", "Storage SAN device stress tests - Storage SAN device stress - mpt3sas driver", "Storage SAN device stress - mpt3sas driver"),
    ("Storage hwraid testing - hw_raid_smartpqi", "Storage hwraid testing - Storage hwraid testing - hw_raid_smartpqi", "Storage hwraid testing - hw_raid_smartpqi"),
    ("Storage hwraid testing - hw_raid_hpsa", "Storage hwraid testing - Storage hwraid testing - hw_raid_hpsa", "Storage hwraid testing - hw_raid_hpsa"),
    ("storage: iSCSI parameters", "Storage iSCSI parameters tests - storage: iSCSI parameters", "storage: iSCSI parameters"),
    ("lvm thinp sanity", "sanity test for lvm thinp - lvm thinp sanity", "lvm thinp sanity"),
    ("storage: SCSI VPD", "Storage SCSI VPD Tests - storage: SCSI VPD", "storage: SCSI VPD"),
    ("storage: software RAID testing", "Storage Software RAID Tests - storage: software RAID testing", "storage: software RAID testing"),
    ("stress: stress-ng", "stress: stress-ng test - stress: stress-ng", "stress: stress-ng"),
    ("Syscalls: nrdiff", "Syscalls: nrdiff - Syscalls: nrdiff", "Syscalls: nrdiff"),
    ("Test eBPF tracing by using kernel selftest", "Test eBPF tracing by using kernel selftest - Test eBPF tracing by using kernel selftest", "Test eBPF tracing by using kernel selftest"),
    ("eBPF tracing: test_bpf module test", "Test eBPF tracing by using test_bpf module - eBPF tracing: test_bpf module test", "eBPF tracing: test_bpf module test"),
    ("trace: ftrace/tracer", "Enable ftrace tracers test - trace: ftrace/tracer", "trace: ftrace/tracer"),
    ("Tracepoints: operational test", "Tracepoints operational test - Tracepoints: operational test", "Tracepoints: operational test"),
    ("Libhugetlbfs - version 2.2.1", "libhugetlbfs - version 2.2.1 - Libhugetlbfs - version 2.2.1", "Libhugetlbfs - version 2.2.1"),
    ("KVM Self Tests", "KVM Self Tests - KVM Self Tests", "KVM Self Tests"),
    ("KVM Unit Tests", "KVM Unit Tests - KVM Unit Tests", "KVM Unit Tests"),
]

def fix_tests_with_wrong_url(apps, schema_editor):
    """
    There are some tests duplicated because the fetch_url changed temporarily.

    Merge them into the instance with the github url.
    """
    db_alias = schema_editor.connection.alias
    TestMgr = apps.get_model('datawarehouse', 'Test').objects.using(db_alias)
    TestRunMgr = apps.get_model('datawarehouse', 'TestRun').objects.using(db_alias)

    # First fix one dangling test ¯\_(ツ)_/¯
    # This one cannot be bulk-fixed because correct.name != wrong.name
    # Not really sure why though. Timing between changes maybe?
    wrong = {'name': 'fwts firmware test suite',
             'fetch_url': 'git://git.engineering.redhat.com/users/rasibley/CKI-project-tests-beaker.git#firmware/user-specified'}
    correct = {'name': 'Firmware test suite',
               'fetch_url': 'git://git.engineering.redhat.com/users/rasibley/CKI-project-tests-beaker.git#firmware/user-specified'}

    if TestMgr.filter(**wrong).exists():
        wrong_test = TestMgr.get(**wrong)
        correct_test = TestMgr.get(**correct)
        TestRunMgr.filter(test=wrong_test).update(test=correct_test)
        wrong_test.delete()

    # Fix the others.
    # Look for tests with same name but different url. Merge them into the one that
    # has an url in github.com
    for wrong_test in TestMgr.filter(fetch_url__contains='git.engineering.redhat.com'):
        correct_test = TestMgr.exclude(id=wrong_test.id).get(name=wrong_test.name,
                                                             fetch_url__contains='github.com')
        TestRunMgr.filter(test=wrong_test).update(test=correct_test)
        wrong_test.delete()


def update_tests_name(apps, schema_editor):
    """
    Fix name changes.

    Some kpet-db changes modified the tests' names (twice).
    Grab the table of name equivalences and merge them all into
    the oldest (lowest id) instance.
    """
    db_alias = schema_editor.connection.alias
    TestMgr = apps.get_model('datawarehouse', 'Test').objects.using(db_alias)
    TestRunMgr = apps.get_model('datawarehouse', 'TestRun').objects.using(db_alias)

    for names in names_change_list:
        tests = TestMgr.filter(name__in=names).order_by('id')

        if len(tests) <= 1:
            # No need to merge.
            continue

        test_to_keep = tests[0]
        tests_to_delete = tests[1:]

        for test_to_delete in tests_to_delete:
            assert test_to_delete.id > test_to_keep.id
            TestRunMgr.filter(test=test_to_delete).update(test=test_to_keep)
            test_to_delete.delete()

        # Change the test name to the newest.
        test_to_keep.name = names[2]
        test_to_keep.save()


def run_some_checks(apps, schema_editor):
    """Try to check that everything is OK."""
    db_alias = schema_editor.connection.alias
    TestMgr = apps.get_model('datawarehouse', 'Test').objects.using(db_alias)
    TestRunMgr = apps.get_model('datawarehouse', 'TestRun').objects.using(db_alias)

    # These queries should return one result only.
    # (Random poke some tests)
    assert TestMgr.filter(name='LTP: openposix test suite').count() <= 1
    assert TestMgr.filter(name='LTP lite').count() <= 1
    assert TestMgr.filter(name='Storage blktests').count() <= 1
    assert TestMgr.filter(name='ACPI table test').count() <= 1

    # No git.engineering.redhat.com tests.
    assert TestMgr.filter(fetch_url__contains='git.engineering').count() == 0

    # Just to be sure.
    assert TestRunMgr.filter(test=None).count() == 0


class Migration(migrations.Migration):
    """
    Fix some issues with the tests names and urls.
    """

    dependencies = [
        ('datawarehouse', '0071_buildrun_duration'),
    ]

    operations = [
        migrations.RunPython(fix_tests_with_wrong_url),
        migrations.RunPython(update_tests_name),
        migrations.RunPython(run_some_checks),
    ]
