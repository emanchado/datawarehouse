from django.db import migrations, models
import django.db.models.deletion


def migrate_Log_to_Artifact(apps, schema_editor):
    """Move all Log instances to Artifact."""
    LintRun = apps.get_model('datawarehouse', 'LintRun')
    MergeRun = apps.get_model('datawarehouse', 'MergeRun')
    BuildRun = apps.get_model('datawarehouse', 'BuildRun')

    Log = apps.get_model('datawarehouse', 'Log')
    Artifact = apps.get_model('datawarehouse', 'Artifact')

    db_alias = schema_editor.connection.alias


    for obj_kind in [LintRun, MergeRun, BuildRun]:
        for obj in obj_kind.objects.using(db_alias).all():
            if obj.log:
                artifact = Artifact.objects.using(db_alias).create(
                    file=obj.log.logfile,
                    name=obj.log.name,
                    pipeline=obj.log.pipeline,
                )
                obj.log_artifact = artifact
                obj.save()
                obj.log.delete() # Delete old log.


class Migration(migrations.Migration):
    """
    Create a temporary log_artifact field to copy the log field turned into an Artifact.

    Later it will be removed.
    """

    dependencies = [
        ('datawarehouse', '0062_testlog_to_artifact'),
    ]

    operations = [
        migrations.AddField(
            model_name='buildrun',
            name='log_artifact',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='datawarehouse.Artifact'),
        ),
        migrations.AddField(
            model_name='lintrun',
            name='log_artifact',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='datawarehouse.Artifact'),
        ),
        migrations.AddField(
            model_name='mergerun',
            name='log_artifact',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='datawarehouse.Artifact'),
        ),
        migrations.RunPython(migrate_Log_to_Artifact),
    ]
