# Generated by Django 2.2.5 on 2019-09-17 16:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0015_patchworkseries_submitted_date'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='patchworkseries',
            options={'ordering': ('-series_id',)},
        ),
        migrations.RemoveField(
            model_name='mergerun',
            name='patches',
        ),
        migrations.AddField(
            model_name='patch',
            name='pipelines',
            field=models.ManyToManyField(related_name='patches', to='datawarehouse.Pipeline'),
        ),
    ]
