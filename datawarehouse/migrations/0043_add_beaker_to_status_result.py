# Generated by Django 2.2.7 on 2019-11-19 15:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0042_test_status_result_rename'),
    ]

    operations = [
        migrations.RenameField(
            model_name='testrun',
            old_name='result',
            new_name='beaker_result',
        ),
        migrations.RenameField(
            model_name='testrun',
            old_name='status',
            new_name='beaker_status',
        ),
    ]
