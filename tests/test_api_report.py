"""Test the report api module."""
import json
import datetime
from pytz import UTC

from django.utils import timezone

from tests import utils
from datawarehouse import models, serializers

EMAIL_COMPLETE = (
    "Received: from smtp.corp.redhat.com (int-mx04.intmail.prod.int.phx2.redhat.com\n\t[10.5.11.14])\n\t"
    "by lists01.intranet.prod.int.phx2.redhat.com (8.13.8/8.13.8) with ESMTP\n\tid 009JXLrh010670\n\tfor"
    " <redacted-email-add@post-office.corp.redhat.com>;\n\tThu, 9 Jan 2020 14:33:21 -0500\nReceived: by "
    "smtp.corp.redhat.com (Postfix)\n\tid 9623D5DA60; Thu,  9 Jan 2020 19:33:21 +0000 (UTC)\nDelivered-T"
    "o: redacted-email-add@redhat.com\nCc: test@redhat.com\nReceived: from [172.54.108.255] (cpt-1042.pa"
    "as.prod.upshift.rdu2.redhat.com\n\t[10.0.19.67])\n\tby smtp.corp.redhat.com (Postfix) with ESMTP id"
    " 09A85D9C9;\n\tThu,  9 Jan 2020 19:33:16 +0000 (UTC)\nContent-Type: text/plain; charset=\"utf-8\"\n"
    "Content-Transfer-Encoding: 7bit\nMIME-Version: 1.0\nFrom: CKI Project <cki-project@redhat.com>\nTo:"
    " redacted-email-add@redhat.com, redacted-email@redhat.com\nSubject: =?utf-8?b?4pyF?= PASS: Re: [RHE"
    "L    PATCH 000000000 2/2] tpm: Revert\n\t\"Lorem ipsum dolor sit amet, consectetur adipisci\'s\"\nD"
    "ate: Thu, 09 Jan 2020 19:33:16 -0000\nIn-Reply-To: <20200109184859.6755-3-redacted@redhat.com>\nMes"
    "sage-ID: <cki.0.5FM2O05IQP@redhat.com>\nX-Gitlab-Pipeline-ID: 374759\nX-Gitlab-Url: https://xci32.l"
    "ab.eng.rdu2.redhat.com\nX-Gitlab-Path: /cki-project/cki-pipeline/pipelines/374759\nX-Scanned-By: MI"
    "MEDefang 2.79 on 10.5.11.14\nX-loop: redacted-email-add@redhat.com\nX-BeenThere: redacted-email-add"
    "@redhat.com\nX-Mailman-Version: 2.1.12\nPrecedence: junk\nList-Id: Results of kernel CI tests in de"
    "velopment environment\n\t<redacted-email-add.redhat.com>\nList-Unsubscribe: <https://post-office.co"
    "rp.redhat.com/mailman/options/redacted-email-add>,\n\t<mailto:redacted-email-add-request@redhat.com"
    "?subject=unsubscribe>\nList-Archive: <http://post-office.corp.redhat.com/archives/skt-results-maste"
    "r>\nList-Post: <mailto:redacted-email-add@redhat.com>\nList-Help: <mailto:redacted-email-add-reques"
    "t@redhat.com?subject=help>\nList-Subscribe: <https://post-office.corp.redhat.com/mailman/listinfo/s"
    "kt-results-master>,\n\t<mailto:redacted-email-add-request@redhat.com?subject=subscribe>\nX-List-Rec"
    "eived-Date: Thu, 09 Jan 2020 19:33:21 -0000\n\n\nHello,\n\nWe ran automated tests on a patchset tha"
    "t was proposed for merging into this\nkernel tree.")

EMAIL_NO_RECIPIENTS = (
    "Received: from smtp.corp.redhat.com (int-mx04.intmail.prod.int.phx2.redhat.com\n\t[10.5.11.14])\n\t"
    "by lists01.intranet.prod.int.phx2.redhat.com (8.13.8/8.13.8) with ESMTP\n\tid 009JXLrh010670\n\tfor"
    " <redacted-email-add@post-office.corp.redhat.com>;\n\tThu, 9 Jan 2020 14:33:21 -0500\nReceived: by "
    "smtp.corp.redhat.com (Postfix)\n\tid 9623D5DA60; Thu,  9 Jan 2020 19:33:21 +0000 (UTC)\nDelivered-T"
    "o: redacted-email-add@redhat.com\nReceived: from [172.54.108.255] (cpt-1042.paas.prod.upshift.rdu2."
    "redhat.com\n\t[10.0.19.67])\n\tby smtp.corp.redhat.com (Postfix) with ESMTP id 09A85D9C9;\n\tThu,  "
    "9 Jan 2020 19:33:16 +0000 (UTC)\nContent-Type: text/plain; charset=\"utf-8\"\nContent-Transfer-Enco"
    "ding: 7bit\nMIME-Version: 1.0\nFrom: CKI Project <cki-project@redhat.com>\nSubject: =?utf-8?b?4pyF?"
    "= PASS:TCH 000000000 2/2] tpm: Revert\n\t\"Lorem ipsum dolor sit amet, consectetur adipisci\'s\"\nD"
    "ate: Thu, 09 Jan 2020 19:33:16 -0000\nIn-Reply-To: <20200109184859.6755-3-redacted@redhat.com>\nMes"
    "sage-ID: <cki.0.5FM2O05IQP@redhat.com>\nX-Gitlab-Pipeline-ID: 374759\nX-Gitlab-Url: https://xci32.l"
    "ab.eng.rdu2.redhat.com\nX-Gitlab-Path: /cki-project/cki-pipeline/pipelines/374759\nX-Scanned-By: MI"
    "MEDefang 2.79 on 10.5.11.14\nX-loop: redacted-email-add@redhat.com\nX-BeenThere: redacted-email-add"
    "@redhat.com\nX-Mailman-Version: 2.1.12\nPrecedence: junk\nList-Id: Results of kernel CI tests in de"
    "velopment environment\n\t<redacted-email-add.redhat.com>\nList-Unsubscribe: <https://post-office.co"
    "rp.redhat.com/mailman/options/redacted-email-add>,\n\t<mailto:redacted-email-add-request@redhat.com"
    "?subject=unsubscribe>\nList-Archive: <http://post-office.corp.redhat.com/archives/skt-results-maste"
    "r>\nList-Post: <mailto:redacted-email-add@redhat.com>\nList-Help: <mailto:redacted-email-add-reques"
    "t@redhat.com?subject=help>\nList-Subscribe: <https://post-office.corp.redhat.com/mailman/listinfo/s"
    "kt-results-master>,\n\t<mailto:redacted-email-add-request@redhat.com?subject=subscribe>\nX-List-Rec"
    "eived-Date: Thu, 09 Jan 2020 19:33:21 -0000\n\n\nHello,\n\nWe ran automated tests on a patchset tha"
    "t was proposed for merging into this\nkernel tree.")

EMAIL_B64_ENCODED = (
    "Received: from smtp.corp.redhat.com (int-mx07.intmail.prod.int.phx2.redhat.com\n       [10.5.11.22]"
    ")\n       by lists01.intranet.prod.int.phx2.redhat.com (8.13.8/8.13.8) with ESMTP\n       id 00B46E"
    "dl016765\n       for <redacted-email-add@post-office.corp.redhat.com>;\n       Fri, 10 Jan 2020 23:"
    "06:14 -0500\nReceived: by smtp.corp.redhat.com (Postfix)\n       id AEA0F1001B00; Sat, 11 Jan 2020 "
    "04:06:14 +0000 (UTC)\nDelivered-To: redacted-email-add@redhat.com\nReceived: from [172.54.91.90] (c"
    "pt-1046.paas.prod.upshift.rdu2.redhat.com\n       [10.0.19.73])\n       by smtp.corp.redhat.com (Po"
    "stfix) with ESMTP id 444EA10013A1;\n       Sat, 11 Jan 2020 04:06:10 +0000 (UTC)\nContent-Type: tex"
    "t/plain; charset=\"utf-8\"\nMIME-Version: 1.0\nFrom: CKI Project <cki-project@rhat.com>\nTo: redact"
    "ed-team@redhat.com\nSubject: =?utf-8?b?4pyF?= PASS: Test report for kernel 5.5.0-rc4-1e123d9.cki\n "
    "(reda)\nDate: Sat, 11 Jan 2020 04:06:09 -0000\nMessage-ID: <cki.D4B58810D2.redacted12@redhat.com>\n"
    "X-Gitlab-Pipeline-ID: 376262\nX-Gitlab-Url: https://xci32.lab.eng.rdu2.redhat.com\nX-Gitlab-Path: /"
    "cki-project/cki-pipeline/pipelines/376262\nX-Scanned-By: MIMEDefang 2.84 on 10.5.11.22\nContent-Tra"
    "nsfer-Encoding: base64\nX-MIME-Autoconverted: from quoted-printable to 8bit by\n       lists01.intr"
    "anet.prod.int.phx2.redhat.com id 00B46Edl016765\nX-loop: redacted-email-add@redhat.com\nX-BeenThere"
    ": redacted-email-add@redhat.com\nX-Mailman-Version: 2.1.12\nPrecedence: junk\nList-Id: Results of k"
    "ernel CI tests in development environment\n       <redacted-email-add.redhat.com>\nList-Unsubscribe"
    ": <https://post-office.corp.redhat.com/mailman/options/redacted-email-add>,\n       <mailto:redacte"
    "d-email-address-123@redhat.com?subject=unsubscribe>\nList-Archive: <http://post-office.corp.redhat."
    "com/archives/redacted-email-add>\nList-Post: <mailto:redacted-email-add@redhat.com>\nList-Help: <ma"
    "ilto:redacted-email-add-request@redhat.com?subject=help>\nList-Subscribe: <https://post-office.corp"
    ".redhat.com/mailman/listinfo/redacted-email-add>,\n       <mailto:redacted-email-add-request@redhat"
    ".com?subject=subscribe>\nX-List-Received-Date: Sat, 11 Jan 2020 04:06:14 -0000\n\nCkhlbGxvLAoKV2Ugc"
    "mFuIGF1dG9tYXRlZCB0ZXN0cyBvbiBhIHJlY2VudCBjb21taXQgZnJvbSB0\naGlzIGtlcm5lbCB0cmVlOgoKICAgICAgIEtlcm"
    "5lbCByZXBvOiBnaXQ6Ly9naXQua2VybmVsLm9y\nZy9wdWIvc2NtL2xpbnV4L2tlcm5lbC9naXQvcmRtYS9yZG1hLmdpdAogICA"
    "gICAgICAgICBDb21t\naXQ6IDFlMTIzZDk2Yjg1ZiAtIFJETUEvY29yZTogUmVtb3ZlIGVyciBpbiBpd19xdWVyeV9wb3J0\nCg"
    "pUaGUgcmVzdWx0cyBvZiB0aGVzZSBhdXRvbWF0ZWQgdGVzdHMgYXJlIHByb3ZpZGVkIGJlbG93\nLgoKICAgIE92ZXJhbGwgcmV"
    "zdWx0OiBQQVNTRUQKICAgICAgICAgICAgIE1lcmdlOiBPSwogICAg\nICAgICAgIENvbXBpbGU6IE9LCiAgICAgICAgICAgICBU"
    "ZXN0czogT0sKCiAgICBQaXBlbGluZTog\naHR0cHM6Ly94Y2kzMi5sYWIuZW5nLnJkdTIucmVkaGF0LmNvbS9ja2ktcHJvamVjd"
    "C9ja2ktcGlw\nZWxpbmUvcGlwZWxpbmVzLzM3NjI2MgoKUGxlYXNlIHJlcGx5IHRvIHRoaXMgZW1haWwgaWYgeW91\nIGhhdmUg"
    "YW55IHF1ZXN0aW9ucyBhYm91dCB0aGUgdGVzdHMgdGhhdCB3ZQpyYW4gb3IgaWYgeW91\nIGhhdmUgYW55IHN1Z2dlc3Rpb25zI"
    "G9uIGhvdyB0byBtYWtlIGZ1dHVyZSB0ZXN0cyBtb3JlIGVm\nZmVjdGl2ZS4KCiAgICAgICAgLC0uICAgLC0uCiAgICAgICAoIE"
    "MgKSAoIEsgKSAgQ29udGludW91\ncwogICAgICAgIGAtJywtLmAtJyAgIEtlcm5lbAogICAgICAgICAgKCBJICkgICAgIEludGV"
    "ncmF0\naW9uCiAgICAgICAgICAgYC0nCl9fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19f\nX19fX19fX19fX1"
    "9fX19fX19fX19fX19fX19fX19fX19fX19fX19fXwoKQ29tcGlsZSB0ZXN0aW5n\nCi0tLS0tLS0tLS0tLS0tLQoKV2UgY29tcGl"
    "sZWQgdGhlIGtlcm5lbCBmb3IgMSBhcmNoaXRlY3R1\ncmU6CgogICAgeDg2XzY0OgogICAgICBtYWtlIG9wdGlvbnM6IC1qMzAg"
    "SU5TVEFMTF9NT0RfU1RS\nSVA9MSB0YXJnei1wa2cKCgpIYXJkd2FyZSB0ZXN0aW5nCi0tLS0tLS0tLS0tLS0tLS0KQWxsIHRo"
    "\nZSB0ZXN0aW5nIGpvYnMgYXJlIGxpc3RlZCBoZXJlOgoKICBodHRwczovL2JlYWtlci5lbmdpbmVl\ncmluZy5yZWRoYXQuY29"
    "tL2pvYnMvP2pvYnNlYXJjaC0wLnRhYmxlPVdoaXRlYm9hcmQmam9ic2Vh\ncmNoLTAub3BlcmF0aW9uPWNvbnRhaW5zJmpvYnNl"
    "YXJjaC0wLnZhbHVlPWNraSU0MGdpdGxhYiUz\nQTM3NjI2MgoKV2UgYm9vdGVkIGVhY2gga2VybmVsIGFuZCByYW4gdGhlIGZvb"
    "Gxvd2luZyB0ZXN0\nczoKCiAgeDg2XzY0OgogICAgSG9zdCAxOiBodHRwczovL2JlYWtlci5lbmdpbmVlcmluZy5yZWRo\nYXQu"
    "Y29tL3JlY2lwZXMvNzc4MDU4NQogICAgICAg4pyFIEJvb3QgdGVzdAogICAgICAg8J+apyDi\nnIUgUkRNQSBTYW5pdHkgVGVzd"
    "CBJQiAtIFNlcnZlcgoKICAgIEhvc3QgMjogaHR0cHM6Ly9iZWFr\nZXIuZW5naW5lZXJpbmcucmVkaGF0LmNvbS9yZWNpcGVzLz"
    "c3ODA1ODYKICAgICAgIOKchSBCb290\nIHRlc3QKICAgICAgIPCfmqcg4pyFIFJETUEgU2FuaXR5IFRlc3QgSUIgLSBDbGllbnQ"
    "KCiAgICBI\nb3N0IDM6IGh0dHBzOi8vYmVha2VyLmVuZ2luZWVyaW5nLnJlZGhhdC5jb20vcmVjaXBlcy83Nzgw\nNTg3CiAgIC"
    "AgICDinIUgQm9vdCB0ZXN0CiAgICAgICDwn5qnIOKchSBSRE1BIFNhbml0eSBUZXN0\nIFJvQ0UgLSBTZXJ2ZXIKCiAgICBIb3N"
    "0IDQ6IGh0dHBzOi8vYmVha2VyLmVuZ2luZWVyaW5nLnJl\nZGhhdC5jb20vcmVjaXBlcy83NzgwNTg4CiAgICAgICDinIUgQm9v"
    "dCB0ZXN0CiAgICAgICDwn5qn\nIOKchSBSRE1BIFNhbml0eSBUZXN0IFJvQ0UgLSBDbGllbnQKCiAgVGVzdCBzb3VyY2VzOiBod"
    "HRw\nczovL2dpdGh1Yi5jb20vQ0tJLXByb2plY3QvdGVzdHMtYmVha2VyCiAgICDwn5KaIFB1bGwgcmVx\ndWVzdHMgYXJlIHdl"
    "bGNvbWUgZm9yIG5ldyB0ZXN0cyBvciBpbXByb3ZlbWVudHMgdG8gZXhpc3Rp\nbmcgdGVzdHMhCgpXYWl2ZWQgdGVzdHMKLS0tL"
    "S0tLS0tLS0tCklmIHRoZSB0ZXN0IHJ1biBpbmNs\ndWRlZCB3YWl2ZWQgdGVzdHMsIHRoZXkgYXJlIG1hcmtlZCB3aXRoIPCfmq"
    "cuIFN1Y2ggdGVzdHMg\nYXJlCmV4ZWN1dGVkIGJ1dCB0aGVpciByZXN1bHRzIGFyZSBub3QgdGFrZW4gaW50byBhY2NvdW50\nL"
    "iBUZXN0cyBhcmUgd2FpdmVkIHdoZW4KdGhlaXIgcmVzdWx0cyBhcmUgbm90IHJlbGlhYmxlIGVu\nb3VnaCwgZS5nLiB3aGVuIH"
    "RoZXkncmUganVzdCBpbnRyb2R1Y2VkIG9yIGFyZQpiZWluZyBmaXhl\nZC4KClRlc3RpbmcgdGltZW91dAotLS0tLS0tLS0tLS0"
    "tLS0KV2UgYWltIHRvIHByb3ZpZGUgYSBy\nZXBvcnQgd2l0aGluIHJlYXNvbmFibGUgdGltZWZyYW1lLiBUZXN0cyB0aGF0IGhh"
    "dmVuJ3QKZmlu\naXNoZWQgcnVubmluZyBhcmUgbWFya2VkIHdpdGgg4o+xLiBSZXBvcnRzIGZvciBub24tdXBzdHJl\nYW0ga2V"
    "ybmVscyBoYXZlCmEgQmVha2VyIHJlY2lwZSBsaW5rZWQgdG8gbmV4dCB0byBlYWNoIGhv\nc3QuCgo=\n")


class APIReportTestCase(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Unit tests for the views module."""

    def setUp(self):
        """Set up tests."""
        self.project = models.Project.objects.create(project_id=0)
        self.git_tree = models.GitTree.objects.create(name='Tree 1')

        cases = [
            # (pipeline_id, duration, created_at hours ago, finished_at hour ago),
            (1001, 123, 24, 23),
            (1002, 123, 20, 4),
            (1003, 123, 100, 23),
            (1004, 123, 2, 1),
        ]

        now = timezone.now()
        for pipeline_id, duration, ca_ha, fa_ha in cases:
            models.Pipeline.objects.create(pipeline_id=pipeline_id, project=self.project,
                                           gittree=self.git_tree, duration=duration,
                                           created_at=(now - datetime.timedelta(hours=ca_ha)),
                                           finished_at=(now - datetime.timedelta(hours=fa_ha)),
                                           )

    def test_report_create(self):
        """Test creating a report."""
        pipeline = models.Pipeline.objects.first()
        email = {
            'content': EMAIL_COMPLETE,
        }

        auth_client = self.get_authenticated_client()
        auth_client.post(
            f'/api/1/pipeline/{pipeline.pipeline_id}/report',
            json.dumps(email), content_type="application/json")

        report = pipeline.reports.first()

        self.assertEqual(
            '\nHello,\n\nWe ran automated tests on a patchset that was proposed for merging into this\nkernel tree.',
            report.body)
        self.assertEqual(
            '=?utf-8?b?4pyF?= PASS: Re: [RHEL    PATCH 000000000 2/2] ' +
            'tpm: Revert\n\t"Lorem ipsum dolor sit amet, consectetur adipisci\'s"',
            report.subject)
        self.assertEqual(
            datetime.datetime(2020, 1, 9, 19, 33, 16, tzinfo=UTC),
            report.sent_at
        )
        self.assertEqual(
            '<cki.0.5FM2O05IQP@redhat.com>',
            report.msgid)

        report.addr_to.get(email='redacted-email-add@redhat.com')
        report.addr_to.get(email='redacted-email@redhat.com')
        report.addr_cc.get(email='test@redhat.com')

    def test_report_create_duplicated(self):
        """Test creating a report two times."""
        pipeline = models.Pipeline.objects.first()
        email = {
            'content': EMAIL_COMPLETE,
        }

        # First time created.
        auth_client = self.get_authenticated_client()
        response = auth_client.post(
            f'/api/1/pipeline/{pipeline.pipeline_id}/report',
            json.dumps(email), content_type="application/json")
        self.assertEqual(201, response.status_code)
        self.assertEqual(1, pipeline.reports.count())

        # Second time not created.
        response = auth_client.post(
            f'/api/1/pipeline/{pipeline.pipeline_id}/report',
            json.dumps(email), content_type="application/json")
        self.assertEqual(400, response.status_code)
        self.assertEqual(b'"MsgID already exists."', response.content)
        self.assertEqual(1, pipeline.reports.count())

    def test_report_get(self):
        """Test getting pipeline's reports."""
        pipeline = models.Pipeline.objects.first()
        pipeline.reports.create(
            body='content',
            subject='subject',
            sent_at=timezone.now(),
            msgid='<someting@redhat.com>',
        )

        response = self.client.get(f'/api/1/pipeline/{pipeline.pipeline_id}/report')

        self.assertEqual(
            response.json()['results'],
            {
                'reports': serializers.ReportSerializer(pipeline.reports.all(), many=True).data,
            }
        )

    def test_report_create_emails_empty(self):
        """Test creating a report with empty email fields."""
        pipeline = models.Pipeline.objects.first()
        email = {
            'content': EMAIL_NO_RECIPIENTS,
        }

        auth_client = self.get_authenticated_client()
        auth_client.post(
            f'/api/1/pipeline/{pipeline.pipeline_id}/report',
            json.dumps(email), content_type="application/json")

        report = pipeline.reports.first()

        self.assertEqual(0, report.addr_to.count())
        self.assertEqual(0, report.addr_cc.count())

    def test_create_encoded(self):
        """Test creating report with body encoded."""
        pipeline = models.Pipeline.objects.first()
        email = {
            'content': EMAIL_B64_ENCODED,
        }

        auth_client = self.get_authenticated_client()
        auth_client.post(
            f'/api/1/pipeline/{pipeline.pipeline_id}/report',
            json.dumps(email), content_type="application/json")

        report = pipeline.reports.first()

        self.assertEqual(
            '\nHello,\n\nW',
            report.body[:10])

    def test_missing_report(self):
        """Test getting missing reports."""
        response = self.client.get(f'/api/1/report/missing')
        self.assertEqual(
            response.json()['results'],
            {'pipelines': serializers.PipelineSimpleSerializer(
                models.Pipeline.objects.filter(pipeline_id__in=[1001, 1002, 1003]),
                many=True).data
             }
        )

    def test_missing_report_since(self):
        """Test getting missing reports with since filter."""
        since = datetime.datetime.now() - datetime.timedelta(hours=48)

        response = self.client.get(f'/api/1/report/missing?since={since}')
        self.assertEqual(
            response.json()['results'],
            {'pipelines': serializers.PipelineSimpleSerializer(
                models.Pipeline.objects.filter(pipeline_id__in=[1001, 1002]),
                many=True).data
             }
        )

    def test_missing_report_since_with_report(self):
        """Test getting missing reports with since filter."""
        since = datetime.datetime.now() - datetime.timedelta(hours=48)

        pipeline = models.Pipeline.objects.get(pipeline_id=1001)
        pipeline.reports.create(
            body='content',
            subject='subject',
            sent_at=timezone.now(),
            msgid='<someting@redhat.com>',
        )

        response = self.client.get(f'/api/1/report/missing?since={since}')
        self.assertEqual(
            response.json()['results'],
            {'pipelines': serializers.PipelineSimpleSerializer(
                models.Pipeline.objects.filter(pipeline_id__in=[1002]),
                many=True).data
             }
        )
